#!/bin/sh
yum install -y postgresql postgresql-server postgresql-devel
yum install -y httpd screen wget
yum install -y gcc python-devel
cd /tmp && curl -O https://bootstrap.pypa.io/get-pip.py
python /tmp/get-pip.py
rm /tmp/get-pip.py
pip install momoko
pip install simplejson
pip install tornado
postgresql-setup initdb
sed -i /var/lib/pgsql/data/pg_hba.conf -e "s^host    all             all             127.0.0.1/32            ident^host    all             all             127.0.0.1/32            trust^"
sed -i /var/lib/pgsql/data/pg_hba.conf -e "s^host    all             all             ::1/128                 ident^host    all             all             ::1/128                 trust^"
service postgresql start
chkconfig postgresql on
su - postgres -c "createuser opensignups"
su - postgres -c "createdb opensignups"
#useradd opensignups
#echo $1 | passwd --stdin opensignups
cat << EOF > /etc/httpd/conf.d/webtimer.conf
ProxyRequests Off
ProxyPreserveHost On
ProxyPass / http://localhost:8000/
ProxyPassReverse / http://localhost:8000/
<Proxy *>
    Order deny,allow
    Allow from all
</Proxy>
EOF
firewall-cmd --permanent --zone=public --add-service=http
# --- FIX THIS
service firewalld stop
/usr/sbin/setsebool -P httpd_can_network_connect 1
service httpd start
chkconfig httpd on
cat << EOF > /home/opensignups/start.sh
#!/bin/sh
git clone https://opensignups:$1@bitbucket.org/opensignups/webtimer.git webtimer
cd webtimer && psql < schema.sql && nohup python web_timer.py production.conf &
EOF
chmod +x /home/opensignups/start.sh
chown opensignups:opensignups /home/opensignups/start.sh
su - opensignups -c "./start.sh"